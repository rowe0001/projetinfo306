package com.example.timetablehelper;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.timetablehelper.storage.DatabaseHandler;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setElevation(0);
            actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.app_main_actionbar));
        }

        setContentView(R.layout.activity_settings);

        if(getIntent().getExtras() != null) {
            String url = getIntent().getExtras().getString("url");
            EditText editText = (EditText) findViewById(R.id.settingsEditText);

            if(url != null) {
                editText.setHint(url);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, MainActivity.class);

        if(getIntent().getExtras() != null) {
           String uriString = getIntent().getExtras().getString("uri");
            if(uriString != null) {
                intent.setData(Uri.parse(uriString));
            }
        }

        EditText editText = (EditText) findViewById(R.id.settingsEditText);
        DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
        if(editText.getText().toString() != null && !editText.getText().toString().equals(""))
            databaseHandler.addSetting("url", editText.getText().toString());

        startActivity(intent);
        return true;
    }
}