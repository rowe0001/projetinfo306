package com.example.timetablehelper.models;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {

    private final String id;
    private final Date created;
    private final Date start;
    private final Date end;
    private String title;
    private String description;
    private String location;

    public Event(String id, Date created, Date start, Date end, String title, String description, String location) {
        this.id = id;
        this.created = created;
        this.start = start;
        this.end = end;
        this.title = title;
        this.description = description;
        this.location = location;
    }

    public String getId() {
        return this.id;
    }
    public Date getCreated() {
        return this.created;
    }
    public Date getStart() {
        return this.start;
    }
    public Date getEnd() {
        return this.end;
    }
    public String getTitle() {
        return this.title;
    }
    public String getDescription() {
        return this.description;
    }
    public String getLocation() {
        return this.location;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setLocation(String location) { this.location = location; }

    @NonNull
    @Override
    public String toString() {

        return "id=" + this.id + " " +
        "created=" + this.created + " " +
        "start=" + this.start + " " +
        "end=" + this.end + " " +
        "title=" + this.title + " " +
        "description=" + this.description + " " +
        "location=" + this.location;
    }
}
