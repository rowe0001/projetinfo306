package com.example.timetablehelper.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.timetablehelper.MainActivity;
import com.example.timetablehelper.R;
import com.example.timetablehelper.models.Event;
import com.example.timetablehelper.storage.DatabaseHandler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class NotificationService extends Service {
    private final String SERVICE_TAG = "SERVICE";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(SERVICE_TAG, "onCreate");

        new Thread(() -> {
            DatabaseHandler handler = new DatabaseHandler(getApplicationContext());
            ArrayList<String> sentEventIds = new ArrayList<>();
            ArrayList<Event> events = null;

            try {
                events = handler.getEvents(getResources().getConfiguration().getLocales().get(0));
            } catch (ParseException e) {
                events = null;
            }

            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                if(events == null) {
                    return;
                }
                Log.i(SERVICE_TAG, "Searching... found " + events.size() + " event(s).");

                Date date = new Date();
                for(Event event : events) {
                    if(date.compareTo(event.getStart()) > 0 && !sentEventIds.contains(event.getId())) {
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, MainActivity.NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.app_icon)
                            .setContentTitle(event.getTitle())
                            .setContentText(event.getDescription() + " | " + event.getLocation())
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                        NotificationManager notificationManager = getSystemService(NotificationManager.class);
                        notificationManager.notify(sentEventIds.size(), builder.build());

                        sentEventIds.add(event.getId());
                    }
                }
            }
        }).start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(SERVICE_TAG, "onDestroy");
        super.onDestroy();
    }
}
