package com.example.timetablehelper.utils;

import android.content.ContentResolver;
import android.net.Uri;

import com.example.timetablehelper.models.Event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** @noinspection SpellCheckingInspection*/
public class ICSUtils {
    private final ContentResolver resolver;
    private final Uri data;
    private final Locale lang;
    public ICSUtils(ContentResolver resolver, Uri data, Locale lang) {
        this.resolver = resolver;
        this.data = data;
        this.lang = lang;
    }
    private Event fromList(ArrayList<String> list) throws ParseException {
        Event event;

        String id = null;
        Date created = null;
        Date start = null;
        Date end = null;
        String title = null;
        String description = null;
        String location = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss", this.lang);

        for(int i = 0; i < list.size(); i++) {
            Matcher timeZoneMatcher = Pattern.compile("TZID=([^:]+)").matcher(list.get(i));
            Matcher utcMatcher = Pattern.compile(":([0-9]+T[0-9]+)").matcher(list.get(i));

            if(list.get(i).startsWith("DTSTART;")) {
                String timeZone = null, utc = null;

                if(timeZoneMatcher.find()) {
                    timeZone = timeZoneMatcher.group(1);
                }
                if(utcMatcher.find()) {
                    utc = utcMatcher.group(1);
                }

                if(timeZone != null && utc != null) {
                    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
                    start = dateFormat.parse(utc);
                }
            }

            if(list.get(i).startsWith("DTEND;")) {
                String timeZone = null, utc = null;

                if(timeZoneMatcher.find()) {
                    timeZone = timeZoneMatcher.group(1);
                }
                if(utcMatcher.find()) {
                    utc = utcMatcher.group(1);
                }

                if(timeZone != null && utc != null) {
                    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

                    end = dateFormat.parse(utc);
                }
            }

            if(list.get(i).startsWith("DTSTAMP;")) {
                String timeZone = null, utc = null;

                if(timeZoneMatcher.find()) {
                    timeZone = timeZoneMatcher.group(1);
                }
                if(utcMatcher.find()) {
                    utc = utcMatcher.group(1);
                }

                if(timeZone != null && utc != null) {
                    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

                    created = dateFormat.parse(utc);
                }
            }

            if(list.get(i).startsWith("LOCATION:")) {
                String[] split = list.get(i).split(":");
                location = split.length == 2 ? split[1] : null;
            }

            if(list.get(i).startsWith("UID:")) {
                String[] split = list.get(i).split(":");
                id = split.length == 2 ? split[1] : null;
            }

            if(list.get(i).startsWith("SUMMARY:")) {
                String[] split = list.get(i).split(":");
                title = split.length == 2 ? split[1] : null;
            }

            if(list.get(i).startsWith("DESCRIPTION:")) {
                String[] split = list.get(i).split(":");
                description = split.length == 2 ? split[1] : null;
            }
        }

        event = new Event(id, created, start, end, title, description, location);
        dateFormat = new SimpleDateFormat("dd MMM HH:mm", this.lang);

        // Event is a course
        if(event.getLocation() != null) {
            String[] parts = event.getTitle().split("\\\\;");

            String subject = parts[0].trim();
            String courseType = parts[1].trim();
            String teacher = parts[2].trim();

            if (!courseType.startsWith("[") && !courseType.endsWith("]")) {
                courseType = "[" + courseType + "]";
            }

            event.setTitle(courseType + " " + subject.toUpperCase() + " " + teacher);
            event.setDescription(dateFormat.format(event.getStart()) + " – " + dateFormat.format(event.getEnd()));
        } else {
            event.setLocation("-");
            event.setDescription(dateFormat.format(event.getStart()));
        }

        // Special Divers Case
        if(event.getTitle().toLowerCase().startsWith("divers")) {
            String[] parts = event.getTitle().split("\\\\;");

            String subject = parts[0].trim();
            String courseType = parts[2].trim();

            if (!subject.startsWith("[") && !subject.endsWith("]")) {
                subject = "[" + subject + "]";
            }

            event.setTitle(subject.toUpperCase() + " " + courseType);
            event.setDescription(dateFormat.format(event.getStart()) + " – " + dateFormat.format(event.getEnd()));
        }

        Date date = new Date();
        if(date.compareTo(event.getStart()) < 0) {
            return event;
        }

        return null;
    }
    public ArrayList<Event> parse() throws ParseException, IOException {
        InputStream fileData = this.resolver.openInputStream(this.data);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileData));

        boolean save = false;
        ArrayList<String> event = new ArrayList<>();
        ArrayList<Event> events = new ArrayList<>();

        while (reader.ready()) {
            String line = reader.readLine();
            if(line.contains("BEGIN:VEVENT") || save) {
                event.add(line);
                save = true;
            }

            if(line.contains("END:VEVENT")) {
                Event current = this.fromList(event);
                if(current != null)
                    events.add(current);

                event = new ArrayList<>();
                save = false;
            }
        }

        // Sort courses by chronological order
        events.sort(Comparator.comparing(Event::getStart));
        if(events.size() == 0) { return null; }
        return events;
    }

    public ArrayList<Event> parse(URL url) throws IOException, ParseException {
        ArrayList<String> results = new ArrayList<>();
        HttpURLConnection urlConnection = null;

        urlConnection = (HttpURLConnection) url.openConnection();

        InputStream inputStream = urlConnection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        boolean save = false;

        String line;
        ArrayList<String> event = new ArrayList<>();
        ArrayList<Event> events = new ArrayList<>();

        while ((line = reader.readLine()) != null) {
            if(line.contains("BEGIN:VEVENT") || save) {
                event.add(line);
                save = true;
            }

            if(line.contains("END:VEVENT")) {
                Event current = this.fromList(event);
                if(current != null)
                    events.add(current);

                event = new ArrayList<>();
                save = false;
            }
        }

        if (urlConnection != null) {
            urlConnection.disconnect();
        }

        // Sort courses by chronological order
        events.sort(Comparator.comparing(Event::getStart));
        if(events.size() == 0) { return null; }
        return events;
    }
}
