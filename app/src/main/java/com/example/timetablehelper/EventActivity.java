package com.example.timetablehelper;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.timetablehelper.fragments.EventDetailFragment;
import com.example.timetablehelper.fragments.MainBottomFragment;
import com.example.timetablehelper.models.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class EventActivity extends AppCompatActivity {

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(@NonNull Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            Bundle fragmentBundle = new Bundle();
            fragmentBundle.putString("title", "Distance");
            fragmentBundle.putString("description",
                    calculateDistance(latitude, longitude, 49.2438056, 4.0627778) + "m"
            );

            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.eventFragmentViewDistance, EventDetailFragment.class, fragmentBundle)
                    .commit();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Event event = null;
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            event = bundle.getSerializable("event", Event.class);

        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setElevation(0);
            actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.app_main_actionbar));
        }

        if (savedInstanceState == null && event != null) {
            String titleKey = "title";
            String descriptionKey = "description";

            int[] fragments = new int[]{
                R.id.eventFragmentViewId,
                R.id.eventFragmentViewTitle,
                R.id.eventFragmentViewDescription,
                R.id.eventFragmentViewLocation,
                R.id.eventFragmentViewCreated,
                R.id.eventFragmentViewStart,
                R.id.eventFragmentViewEnd
            };

            String[] titles = new String[]{
                getResources().getString(R.string.app_event_detail_id),
                getResources().getString(R.string.app_event_detail_title),
                getResources().getString(R.string.app_event_detail_description),
                getResources().getString(R.string.app_event_detail_location),
                getResources().getString(R.string.app_event_detail_created),
                getResources().getString(R.string.app_event_detail_start),
                getResources().getString(R.string.app_event_detail_end)
            };

            SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE d MMMM yyyy HH:mm:ss", getResources().getConfiguration().getLocales().get(0));
            String[] descriptions = new String[]{
                event.getId(),
                event.getTitle(),
                event.getDescription(),
                event.getLocation(),
                dateFormat.format(event.getCreated()),
                dateFormat.format(event.getStart()),
                dateFormat.format(event.getEnd())
            };

            for(int i = 0; i < 7; i++) {
                Bundle fragmentBundle = new Bundle();
                fragmentBundle.putString(titleKey, titles[i]);
                fragmentBundle.putString(descriptionKey, descriptions[i]);

                getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(fragments[i], EventDetailFragment.class, fragmentBundle)
                    .commit();
            }
        }

        setContentView(R.layout.activity_event);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 2);
            return;
        }

        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putString("title", "Distance");
        fragmentBundle.putString("description", "-");

        getSupportFragmentManager().beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.eventFragmentViewDistance, EventDetailFragment.class, fragmentBundle)
            .commit();


        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.event_menu_items, menu);

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.removeUpdates(locationListener);

        String uriString = getIntent().getExtras().getString("uri");
        Intent intent = new Intent(this, MainActivity.class);

        Uri data;
        if(uriString != null) {
            data = Uri.parse(uriString);
            intent.setData(data);
        }

        startActivity(intent);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i("PERMISSION", "GRANTED");
            } else {
                Log.i("PERMISSION", "DENIED");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.removeUpdates(locationListener);
    }

    private static int calculateDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
        final double R = 6371000.0;

        double lat1 = Math.toRadians(latitude1);
        double lon1 = Math.toRadians(longitude1);
        double lat2 = Math.toRadians(latitude2);
        double lon2 = Math.toRadians(longitude2);

        double dLat = lat2 - lat1;
        double dLon = lon2 - lon1;

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c;

        return (int) distance;
    }
}