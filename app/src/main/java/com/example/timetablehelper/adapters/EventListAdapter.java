package com.example.timetablehelper.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.timetablehelper.R;
import com.example.timetablehelper.databinding.ListItemEventBinding;
import com.example.timetablehelper.models.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class EventListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Event> events;
    private LayoutInflater inflater;
    private ListItemEventBinding binding;
    public EventListAdapter(Context context, ArrayList<Event> events) {
        this.context = context;
        this.events = events;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.events.size();
    }

    @Override
    public Object getItem(int i) {
        return this.events.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        binding = ListItemEventBinding.inflate(this.inflater);
        binding.setEvent(this.events.get(i));

        return binding.getRoot();
    }
}
