package com.example.timetablehelper.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.timetablehelper.EventActivity;
import com.example.timetablehelper.R;
import com.example.timetablehelper.adapters.EventListAdapter;
import com.example.timetablehelper.models.Event;

import java.util.ArrayList;
import java.util.Date;

public class MainBottomFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_main_bottom_fragment, container, false);
        TextView textView = (TextView) root.findViewById(R.id.mainBottomFragmentTextView);

        if (getArguments() == null) {
            textView.setText(R.string.app_main_title_not_found);
            return root;
        }

        // Deprecated code, I am ashamed. (Although the non-deprecated version of this works, it's extra long)
        ArrayList<Event> events = (ArrayList<Event>) getArguments().getSerializable("events");

        String uriString = getArguments().getString("uri");
        Uri data = uriString == null ? null : Uri.parse(uriString);

        if(events != null) {
            ListView listView = (ListView) root.findViewById(R.id.mainBottomFragmentListView);
            listView.setAdapter(new EventListAdapter(getContext(), events));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getActivity(), EventActivity.class);
                    intent.putExtra("event", events.get(i));
                    if(data != null) {
                        intent.putExtra("uri", data.toString());
                    }

                    startActivity(intent);
                }
            });

        } else {
            textView.setText(R.string.app_main_title_not_found);
        }

        return root;
    }
}
