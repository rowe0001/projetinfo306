package com.example.timetablehelper.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.timetablehelper.R;
import com.example.timetablehelper.databinding.ActivityEventDetailFragmentBinding;

public class EventDetailFragment extends Fragment {

    ActivityEventDetailFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = ActivityEventDetailFragmentBinding.inflate(inflater, container, false);

        String title = requireArguments().getString("title");
        String description = requireArguments().getString("description");

        binding.setTitle(title);
        binding.setDescription(description);

        return binding.getRoot();
    }

}
