package com.example.timetablehelper.fragments;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import com.example.timetablehelper.MainActivity;
import com.example.timetablehelper.databinding.ActivityMainTopFragmentBinding;
import com.example.timetablehelper.models.Event;
import com.example.timetablehelper.services.NotificationService;

public class MainTopFragment extends Fragment {
    ActivityMainTopFragmentBinding binding;
    ActivityResultLauncher<Intent> openFileLauncher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        openFileLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if(result.getResultCode() == RESULT_OK) {
                if(result.getData() != null) {
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.setData(result.getData().getData());

                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = ActivityMainTopFragmentBinding.inflate(inflater, container, false);

        binding.mainTopFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/calendar");

                openFileLauncher.launch(intent);
            }
        });

        return binding.getRoot();
    }
}
