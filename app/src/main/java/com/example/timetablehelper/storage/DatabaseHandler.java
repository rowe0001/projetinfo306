package com.example.timetablehelper.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.timetablehelper.models.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DatabaseHandler extends SQLiteOpenHelper {

    private final static String NAME = "planner";
    private final static int VERSION = 2;
    private final static String EVENT_TABLE_NAME = "events";
    private final static String EVENT_ID_COL = "id";
    private final static String EVENT_TOKEN_COL = "token";
    private final static String EVENT_TITLE_COL = "title";
    private final static String EVENT_DESCRIPTION_COL = "description";
    private final static String EVENT_LOCATION_COL = "location";
    private final static String EVENT_CREATED_COL = "created";
    private final static String EVENT_START_COL = "start";
    private final static String EVENT_END_COL = "end";
    private final static String SETTINGS_TABLE_NAME = "settings";
    private final static String SETTINGS_ID_COL = "id";
    private final static String SETTINGS_NAME_COL = "name";
    private final static String SETTINGS_VALUE_NAME = "value";

    public DatabaseHandler(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query_events = "CREATE TABLE " + EVENT_TABLE_NAME + " (" +
            EVENT_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            EVENT_TOKEN_COL + " TEXT, " +
            EVENT_TITLE_COL + " TEXT, " +
            EVENT_DESCRIPTION_COL + " TEXT, " +
            EVENT_LOCATION_COL + " TEXT, " +
            EVENT_CREATED_COL + " DATETIME, " +
            EVENT_START_COL + " DATETIME, " +
            EVENT_END_COL + " DATETIME);";

        String query_settings = "CREATE TABLE " + SETTINGS_TABLE_NAME + " (" +
            SETTINGS_ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            SETTINGS_NAME_COL + " TEXT, " +
            SETTINGS_VALUE_NAME + " TEXT);";

        sqLiteDatabase.execSQL(query_events);
        sqLiteDatabase.execSQL(query_settings);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int vOld, int vNew) {
        onCreate(sqLiteDatabase);
    }

    public void addEvent(Event event, Locale lang) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", lang);
        String createdDateTime = dateFormat.format(event.getCreated());
        String startDateTime = dateFormat.format(event.getStart());
        String endDateTime = dateFormat.format(event.getEnd());

        ContentValues values = new ContentValues();
        values.put(EVENT_TOKEN_COL, event.getId());
        values.put(EVENT_TITLE_COL, event.getTitle());
        values.put(EVENT_DESCRIPTION_COL, event.getDescription());
        values.put(EVENT_LOCATION_COL, event.getLocation());
        values.put(EVENT_CREATED_COL, createdDateTime);
        values.put(EVENT_START_COL, startDateTime);
        values.put(EVENT_END_COL, endDateTime);

        sqLiteDatabase.insert(EVENT_TABLE_NAME, null, values);
        sqLiteDatabase.close();
    }

    public void overwriteEvents(ArrayList<Event> events, Locale lang) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        String deleteQuery = "DELETE FROM " + EVENT_TABLE_NAME;
        sqLiteDatabase.execSQL(deleteQuery);

        for(Event event : events) {
            this.addEvent(event, lang);
        }
    }

    public ArrayList<Event> getEvents(Locale lang) throws ParseException {
        ArrayList<Event> eventList = new ArrayList<>();
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM " + EVENT_TABLE_NAME, null);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", lang);

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(1);
                String title = cursor.getString(2);
                String description = cursor.getString(3);
                String location = cursor.getString(4);

                String createdString = cursor.getString(5);
                String startString = cursor.getString(6);
                String endString = cursor.getString(7);

                Date created = dateFormat.parse(createdString);
                Date start = dateFormat.parse(startString);
                Date end = dateFormat.parse(endString);

                Event event = new Event(id, created, start, end, title, description, location);
                eventList.add(event);

            } while (cursor.moveToNext());
        }
        cursor.close();

        if(eventList.size() == 0) { return null; }
        return eventList;
    }

    public void addSetting(String name, String value) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        if (settingExists(name)) {
            updateSetting(name, value, sqLiteDatabase);

        } else {
            ContentValues values = new ContentValues();
            values.put(SETTINGS_NAME_COL, name);
            values.put(SETTINGS_VALUE_NAME, value);

            sqLiteDatabase.insert(SETTINGS_TABLE_NAME, null, values);
            sqLiteDatabase.close();
        }
    }

    private boolean settingExists(String name) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(
                SETTINGS_TABLE_NAME,
                new String[]{SETTINGS_NAME_COL},
                SETTINGS_NAME_COL + "=?",
                new String[]{name},
                null,
                null,
                null,
                null
        );

        boolean exists = cursor.getCount() > 0;

        cursor.close();
        return exists;
    }

    private void updateSetting(String name, String value, SQLiteDatabase sqLiteDatabase) {
        ContentValues values = new ContentValues();
        values.put(SETTINGS_VALUE_NAME, value);

        sqLiteDatabase.update(
                SETTINGS_TABLE_NAME,
                values,
                SETTINGS_NAME_COL + "=?",
                new String[]{name}
        );

        sqLiteDatabase.close();
    }

    public String getSetting(String name) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query(
                SETTINGS_TABLE_NAME,
                new String[]{SETTINGS_VALUE_NAME},
                SETTINGS_NAME_COL + "=?",
                new String[]{name},
                null,
                null,
                null,
                null
        );

        String value = null;

        if (cursor.moveToFirst()) {
            value = cursor.getString(0);
        }

        cursor.close();
        sqLiteDatabase.close();

        return value;
    }
}
