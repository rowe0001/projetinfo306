package com.example.timetablehelper;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.timetablehelper.fragments.MainBottomFragment;
import com.example.timetablehelper.models.Event;
import com.example.timetablehelper.services.NotificationService;
import com.example.timetablehelper.storage.DatabaseHandler;
import com.example.timetablehelper.utils.ICSUtils;
import com.example.timetablehelper.utils.NetworkUtils;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final String NOTIFICATION_CHANNEL_ID = "course_channel";
    public static final int NOTIFICATION_CHANNEL_NAME = R.string.notification_channel_name;
    public static final int NOTIFICATION_CHANNEL_DESCRIPTION = R.string.notification_channel_description;

    private static ArrayList<Event> currentEvents;
    private static Uri currentUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Uri data = getIntent().getData();
        Locale lang = getResources().getConfiguration().getLocales().get(0);
        Context context = getApplicationContext();

        DatabaseHandler databaseHandler = new DatabaseHandler(context);
        String timetableStringURL = databaseHandler.getSetting("url");
        if(timetableStringURL == null) {
            databaseHandler.addSetting("url", "https://caldav.univ-reims.fr/URCA/cache/PA3LYDLS133837.ics");
        }

        boolean loadSaved = false;

        // Threaded Work because ICS files can take a long time to process.
        if (data != null && savedInstanceState == null) {
            new Thread(() -> {
                ArrayList<Event> events;

                try {
                    events = new ICSUtils(getContentResolver(), data, lang).parse();
                } catch (ParseException | IOException e) {
                    throw new RuntimeException(e);
                }

                if (events == null) {
                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putSerializable("events", events);
                bundle.putString("uri", data.toString());

                getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.mainBottomFragment, MainBottomFragment.class, bundle)
                    .commit();

                MainActivity.currentEvents = events;
                MainActivity.currentUri = data;
            }).start();
        }

        // Try to retrieve data from internet
        if(data == null && savedInstanceState == null && NetworkUtils.isOnline(context)) {
            new Thread(() -> {
                ArrayList<Event> events;

                try {
                    events = new ICSUtils(getContentResolver(), null, lang).parse(new URL(timetableStringURL));
                } catch (IOException | ParseException e) {
                    events = null;
                }

                if(events == null) {
                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putSerializable("events", events);

                getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.mainBottomFragment, MainBottomFragment.class, bundle)
                    .commit();

                MainActivity.currentEvents = events;
                MainActivity.currentUri = null;
                databaseHandler.overwriteEvents(events, lang);
            }).start();

        } else if(data == null && savedInstanceState == null && !NetworkUtils.isOnline(context)) {
            ArrayList<Event> savedEvents;

            try {
                savedEvents = databaseHandler.getEvents(lang);
            } catch (ParseException e) {
                savedEvents = null;
            }

            if(savedEvents != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("events", savedEvents);

                getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.mainBottomFragment, MainBottomFragment.class, bundle)
                    .commit();

                loadSaved = true;
            }
        }

        if (savedInstanceState == null && !loadSaved) {
            getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.mainBottomFragment, MainBottomFragment.class, new Bundle())
                .commit();
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setElevation(0);
            actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.app_main_actionbar));
        }

        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.POST_NOTIFICATIONS}, 1);
        }

        NotificationChannel notificationChannel = new NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                getResources().getString(NOTIFICATION_CHANNEL_NAME),
                NotificationManager.IMPORTANCE_DEFAULT
        );
        notificationChannel.setDescription(getResources().getString(NOTIFICATION_CHANNEL_DESCRIPTION));

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);

        startService(new Intent(this, NotificationService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu_items, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Context context = getApplicationContext();
        DatabaseHandler dbHandler = new DatabaseHandler(context);
        Locale lang = getResources().getConfiguration().getLocales().get(0);

        if(id == R.id.mainSaveItem) {
            dbHandler.overwriteEvents(MainActivity.currentEvents, lang);
            Toast.makeText(context,
                MainActivity.currentEvents.size() + " " + getResources().getString(R.string.app_toast_save), Toast.LENGTH_SHORT
            ).show();

        } else if(id == R.id.mainSettingsItem) {
            Intent intent = new Intent(this, SettingsActivity.class);
            if(MainActivity.currentUri != null) {
                intent.putExtra("uri", currentUri.toString());
            }
            intent.putExtra("url", dbHandler.getSetting("url"));
            startActivity(intent);
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i("PERMISSION", "GRANTED");
            } else {
                Log.i("PERMISSION", "DENIED");
            }
        }
    }
}